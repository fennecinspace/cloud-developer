export const config = {
  "dev": {
		"username": "postgres",
		"password": "postgres",
		"database": "postgres",
		"host": "postgres.cd0kfldakfpo.us-east-1.rds.amazonaws.com",
		"dialect": "postgres",
		"aws_region": "us-east-1",
		"aws_profile": "default",
    "aws_media_bucket": "udagram-ruttner-dev"
  },
  "prod": {
    "username": "",
    "password": "",
    "database": "udagram_prod",
    "host": "",
    "dialect": "postgres"
  }
}
